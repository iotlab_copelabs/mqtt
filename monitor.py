""" MONITORS THE EXPERIENCE """
import sys
import math
import os

sender_log = []
receiver_log = []

if len(sys.argv) > 1:
    experiment_file_name = sys.argv[1]
else:
    experiment_file_name = "monitor"
#print(experiment_file_name)

if os.path.exists(experiment_file_name + ".txt"):
    os.remove(experiment_file_name + ".txt")

#Get variables from sender_log file
with open("sender_log.txt","r") as s:
    for line in s:
        line = line.rstrip("\n")
        sender_log.append(line)

#Variables from sender
msgs_sent = int(sender_log[0])

with open("receiver_log.txt","r") as r:
    for line in r:
        line = line.rstrip("\n")
        receiver_log.append(line)

#Variables from receiver
msgs_received = int(receiver_log[0])
ttc_avg = receiver_log[1]

# Packet Loss Calculation
packetloss = 100 - (100 * round(msgs_received/float(msgs_sent), 4))


# Analisys:
print("------------ ANALISYS ------------")
print("Messages Received:\t" + str(msgs_received))
print("Messages Sent:\t\t" + str(msgs_sent))
print("Packet Loss:\t\t" + str(packetloss) + "%")
print("TTC Average:\t\t" + str(ttc_avg) + "ms")

f = open(experiment_file_name + ".txt","a+")
f.write("%s\n" % str(msgs_received))
f.write("%s\n" % str(msgs_sent))
f.write("%s\n" % str(packetloss))
f.write("%s\n" % str(ttc_avg))
f.close()
