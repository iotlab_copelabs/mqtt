#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import time
import sys
import json
import os
import psutil

#Clean log file
if os.path.exists("receiver_log.txt"):
    os.remove("receiver_log.txt")

if len(sys.argv) > 1:
    ttc_individual_file_name = sys.argv[1]
else:
    ttc_individual_file_name = "ttc_file"

if os.path.exists(ttc_individual_file_name + ".txt"):
    os.remove(ttc_individual_file_name + ".txt")

# ----------- CONNECTION VARIABLES-----------

# This is the Subscriber
#broker = "12.0.0.246" # - raspberry Pi
broker = "12.0.0.170" # - broker/gw
#broker = "12.0.0.138" # - pc1
topic="sensors"
port = 1882

# ----------- Global variables -----------

timeout = 5 # if no messages are received for timeout seconds, the program stops
start = 0
ttc_array = []
ttc_array_timestamp = []
elapsed = 0
msgs_received = 0
timestamp_send = 0 
ttc_avg = 0
cpu_usage_start = 0
cpu_usage_finish = 0

def cpu_usage():
    return psutil.cpu_times().user

def writeTTC(ttc):
    f = open(ttc_individual_file_name + ".txt","a+")
    f.write("%s\n" % str(ttc))
    f.close()

def on_message(client, userdata, message):
    global start
    global ttc_array_timestamp
    global ttc_array
    global msgs_received
    global timestamp_send
    global cpu_usage_start

    if msgs_received == 0:
        cpu_usage_start = cpu_usage()

    #If a msg is received we start counting time again
    #elapsed = 0
    start = time.time()
    time.clock()

    msgs_received += 1
    payload = str(message.payload.decode("utf-8"))
    msgs = json.loads(payload)

    #TTC (Time To Completion):
    ts = time.time() * 1000 # from s to ms
    timestamp_send = msgs["timestamp"]
    ttc = ts - float(timestamp_send) #in mili seconds
    ttc_array_timestamp.append(str(timestamp_send) + "-" + str(ttc))
    ttc_array.append(ttc)

client = mqtt.Client("subscriber")
client.on_message = on_message
client.connect(broker, port)
client.subscribe(topic)
start = time.time()
time.clock()
print("Subscribed, now waiting for messages ...")
#If no message arives before timeout, starts analisys
while elapsed != timeout:
    elapsed = int(time.time() - start)
    client.loop()
client.loop_stop()

cpu_usage_finish = cpu_usage()
cpu_usage = cpu_usage_finish - cpu_usage_start
print("CPU usage: " + str(cpu_usage))

#After timeout reached, if msgs arived, start calculations
if msgs_received > 0:

    # Write on file
    for r in ttc_array_timestamp:
        writeTTC(r)

    # RTT calculation (average)
    for r in ttc_array:
        #print("Este rtt esta no array: " + str(r))
        ttc_avg += float(r)
    ttc_avg = ttc_avg/len(ttc_array)
    ttc_avg = round(ttc_avg, 3)

    #Write in the log file
    print("messages received: " + str(msgs_received))
    print("ttc average: " + str(ttc_avg) + "ms")

    f = open("receiver_log.txt","a+")
    f.write("%s\n" % str(msgs_received))
    f.write("%s\n" % str(ttc_avg))
    f.close()

else:
    print("Timed out with no messages received")